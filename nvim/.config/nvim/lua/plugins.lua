pcall(require, "impatient")
pcall(require, "packer_compiled")

local status, packer = pcall(require, "plugins.packer")

if not status then
	vim.notify(
		"Packer is not installed. Please install it first from this link https://github.com/wbthomason/packer.nvim",
		"error"
	)
	return
end

local packer_util = require("packer.util")

return packer.startup({
	function(use)
		-- Impatient
		use({
			"lewis6991/impatient.nvim",
			as = "impatient",
		})
		-- Packer
		use({
			"wbthomason/packer.nvim",
			event = "VimEnter",
		})
		-- UI
		use({
			"NTBBloodbath/galaxyline.nvim",
			as = "galaxyline",
			after = { "devicons", "gps" },
			config = function()
				require("plugins.galaxyline")
			end,
		})
		use({
			"akinsho/nvim-bufferline.lua",
			branch = "main",
			as = "bufferline",
			after = "devicons",
			config = function()
				require("plugins.bufferline")
			end,
		})
		-- Theme
		use({
			"sainnhe/gruvbox-material",
			event = "BufEnter",
			config = function()
				require("plugins.gruvbox_material")
			end,
		})
		-- Icons
		use({
			"kyazdani42/nvim-web-devicons",
			as = "devicons",
			after = "gruvbox-material",
			config = function()
				require("plugins.devicons")
			end,
		})
		-- Colors
		use({
			"rrethy/vim-hexokinase",
			run = "make hexokinase",
			as = "hexokinase",
			event = "BufRead",
			setup = require("plugins.setup").hexokinase(),
		})
		-- File managers and pickers
		use({
			"kyazdani42/nvim-tree.lua",
			as = "tree",
			module_pattern = "^nvim%-tree$",
			setup = require("plugins.setup").tree(),
			config = function()
				require("plugins.tree")
			end,
		})
		use({
			"nvim-telescope/telescope.nvim",
			as = "telescope",
			module = "telescope",
			cmd = "Telescope",
			requires = {
				{
					"nvim-telescope/telescope-file-browser.nvim",
					as = "telescope-file-browser",
				},
				{
					"nvim-telescope/telescope-fzf-native.nvim",
					as = "telescope-fzf",
					run = "make",
					module = "fzf_lib",
				},
				{
					"ahmedkhalf/project.nvim",
					as = "project",
					module = "project_nvim",
					config = function()
						require("plugins.project")
					end,
				},
			},
			config = function()
				require("plugins.telescope")
			end,
		})
		-- LSP
		use({
			"neovim/nvim-lspconfig",
			as = "lspconfig",
			event = "BufRead",
		})
		use({
			"williamboman/nvim-lsp-installer",
			as = "lsp-installer",
			after = { "lspconfig", "cmp-lsp", "lsp-signature" },
			config = function()
				require("plugins.lsp_installer")
			end,
		})
		use({
			"jose-elias-alvarez/null-ls.nvim",
			as = "null-ls",
			after = { "plenary", "lspconfig" },
			config = function()
				require("plugins.null_ls")
			end,
		})
		use({
			"ray-x/lsp_signature.nvim",
			as = "lsp-signature",
			event = "BufRead",
		})
		use({
			"onsails/lspkind-nvim",
			as = "lspkind",
			event = "BufRead",
			config = function()
				require("plugins.lspkind")
			end,
		})
		use({
			"hrsh7th/nvim-cmp",
			as = "cmp",
			after = { "luasnip", "lspkind" },
			requires = {
				{
					"hrsh7th/cmp-path",
					after = "cmp",
				},
				{
					"hrsh7th/cmp-calc",
					after = "cmp",
				},
				{
					"hrsh7th/cmp-buffer",
					after = "cmp",
				},
				{
					"hrsh7th/cmp-omni",
					after = "cmp",
				},
				{
					"hrsh7th/cmp-nvim-lsp",
					as = "cmp-lsp",
					after = "cmp",
				},
				{
					"saadparwaiz1/cmp_luasnip",
					as = "cmp-luasnip",
					after = "cmp",
				},
				{
					"hrsh7th/cmp-nvim-lua",
					after = "cmp",
				},
				{
					"hrsh7th/cmp-look",
					after = "cmp",
					config = function()
						require("cmp").register_source("look", require("cmp_look").new())
					end,
				},
				{
					"hrsh7th/cmp-latex-symbols",
					after = "cmp",
				},
				{
					"hrsh7th/cmp-emoji",
					after = "cmp",
				},
			},
			config = function()
				require("plugins.cmp")
			end,
		})
		use({
			"L3MON4D3/LuaSnip",
			as = "luasnip",
			event = "BufRead",
			requires = {
				"rafamadriz/friendly-snippets",
				event = "BufRead",
			},
		})
		use({
			"kosayoda/nvim-lightbulb",
			as = "lightbulb",
			after = "lspconfig",
			config = function()
				require("plugins.lightbulb")
			end,
		})
		use({
			"simrat39/symbols-outline.nvim",
			as = "symbols-outline",
			module = "symbols-outline",
			cmd = { "SymbolsOutline", "SymbolsOutlineOpen" },
			setup = function()
				require("plugins.setup").symbols_outline()
			end,
		})
		-- Treesitter
		use({
			"nvim-treesitter/nvim-treesitter",
			run = ":TSUpdate",
			as = "treesitter",
			event = "BufRead",
			config = function()
				require("plugins.treesitter")
			end,
		})
		use({
			"nvim-treesitter/nvim-treesitter-refactor",
			as = "treesitter-refactor",
			after = "treesitter",
		})
		use({
			"romgrk/nvim-treesitter-context",
			as = "treesitter-context",
			after = "treesitter",
			config = function()
				require("plugins.treesitter_context")
			end,
		})
		use({
			"JoosepAlviste/nvim-ts-context-commentstring",
			branch = "main",
			as = "treesitter-context-commentstring",
			after = "treesitter",
		})
		use({
			"p00f/nvim-ts-rainbow",
			as = "treesitter-rainbow",
			after = "treesitter",
			config = function()
				require("plugins.treesitter_rainbow")
			end,
		})
		use({
			"andymass/vim-matchup",
			as = "matchup",
			after = "treesitter",
			config = function()
				require("plugins.matchup")
			end,
		})
		use({
			"windwp/nvim-autopairs",
			as = "autopairs",
			after = { "cmp", "treesitter" },
			config = function()
				require("plugins.autopairs")
			end,
		})
		use({
			"windwp/nvim-ts-autotag",
			as = "treesitter-autotag",
			after = "treesitter",
		})
		use({
			"SmiteshP/nvim-gps",
			as = "gps",
			after = "treesitter",
			config = function()
				require("plugins.gps")
			end,
		})
		-- Git
		use({
			"lewis6991/gitsigns.nvim",
			branch = "main",
			as = "gitsigns",
			after = "plenary",
			config = function()
				require("plugins.gitsigns")
			end,
		})
		use({
			"tpope/vim-fugitive",
			as = "fugitive",
			cmd = "G*",
		})
		-- Remote
		use({
			"chipsenkbeil/distant.nvim",
			as = "distant",
			cmd = "DistantLaunch",
			config = function()
				require("plugins.nvim")
			end,
		})
		-- Utilities
		use({
			"nvim-lua/plenary.nvim",
			as = "plenary",
			after = "galaxyline",
		})
		use({
			"rcarriga/nvim-notify",
			as = "notify",
			after = "gruvbox-material",
			config = function()
				require("plugins.notify")
			end,
		})
		use({
			"gelguy/wilder.nvim",
			run = ":UpdateRemotePlugins",
			as = "wilder",
			event = "CmdlineEnter",
			config = function()
				require("plugins.wilder")
			end,
		})
		use({
			"beauwilliams/focus.nvim",
			as = "focus",
			module = "focus",
			cmd = { "FocusSplitNicely", "FocusSplitLeft", "FocusSplitDown", "FocusSplitUp", "FocusSplitRight" },
			config = function()
				require("plugins.focus")
			end,
		})
		use({
			"dstein64/nvim-scrollview",
			as = "scrollview",
			event = "BufRead",
			setup = function()
				require("plugins.setup").scrollview()
			end,
		})
		use({
			"goolord/alpha-nvim",
			as = "alpha",
			event = "VimEnter",
			config = function()
				require("plugins.alpha")
			end,
		})
		use({
			"lukas-reineke/indent-blankline.nvim",
			as = "indent-blankline",
			event = "BufRead",
			config = function()
				require("plugins.indent_blankline")
			end,
		})
		use({
			"windwp/nvim-spectre",
			as = "spectre",
			module = "spectre",
			config = function()
				require("spectre").setup({
					live_update = true,
				})
			end,
		})
		use({
			"akinsho/toggleterm.nvim",
			branch = "main",
			as = "toggleterm",
			module = "toggleterm",
			cmd = "ToggleTerm",
			config = function()
				require("plugins.toggleterm")
			end,
		})
		use({
			"folke/persistence.nvim",
			as = "persistence",
			event = "BufReadPre",
			module = "persistence",
			config = function()
				require("persistence").setup()
			end,
		})
		use({
			"numToStr/Comment.nvim",
			as = "comment",
			after = "treesitter-context-commentstring",
			config = function()
				require("plugins.comment")
			end,
		})
		use({
			"wfxr/minimap.vim",
			as = "minimap",
			cmd = "MinimapToggle",
			config = function()
				require("plugins.minimap")
			end,
		})
		use({
			"mg979/vim-visual-multi",
			as = "visual-multi",
			event = "BufRead",
		})
		use({
			"tpope/vim-surround",
			as = "sorround",
			event = "BufEnter",
		})
		use({
			"karb94/neoscroll.nvim",
			as = "neoscroll",
			event = "BufRead",
			config = function()
				require("plugins.neoscroll")
			end,
		})
		use({
			"phaazon/hop.nvim",
			as = "hop",
			module = "hop",
			cmd = { "HopWord", "HopLine" },
			config = function()
				require("hop").setup()
			end,
		})
		use({
			"machakann/vim-highlightedyank",
			as = "highlightedyank",
			event = "BufRead",
		})
		use({
			"folke/todo-comments.nvim",
			as = "todo-comments",
			after = "plenary",
			config = function()
				require("plugins.todo_comments")
			end,
		})
		use({
			"gpanders/editorconfig.nvim",
			as = "editorconfig",
			event = "BufRead",
		})
	end,
	-- Config
	config = {
		-- Move to lua dir so impatient.nvim can cache it
		compile_path = packer_util.join_paths(vim.fn.stdpath("config"), "lua", "packer_compiled.lua"),
	},
})
