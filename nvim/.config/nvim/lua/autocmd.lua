vim.api.nvim_command("autocmd FileType alpha set showtabline=0 | autocmd BufUnload <buffer> set showtabline=2")
vim.api.nvim_command("autocmd FileType alpha set laststatus=0 | autocmd BufUnload <buffer> set laststatus=2")
vim.api.nvim_command("autocmd FileType TelescopePrompt set showtabline=0 | autocmd WinLeave <buffer> set showtabline=2")
