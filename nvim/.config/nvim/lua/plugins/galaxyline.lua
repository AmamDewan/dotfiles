local gl = require("galaxyline")
local glc = require("galaxyline.condition")
local diagnostic = require("galaxyline.providers.diagnostic")
local vcs = require("galaxyline.providers.vcs")
local fileinfo = require("galaxyline.providers.fileinfo")
local extension = require("galaxyline.providers.extensions")
local buffer = require("galaxyline.providers.buffer")
local whitespace = require("galaxyline.providers.whitespace")
local lspclient = require("galaxyline.providers.lsp")
local gps = require("nvim-gps")

local colors = require("theme").colors

local gls = gl.section

local mode_table = {
	[110] = {
		text = "NORMAL",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.gray_light,
		color_bg2 = colors.gray_dark,
	},
	[105] = {
		text = "INSERT",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.blue_light,
		color_bg2 = colors.blue,
	},
	[82] = {
		text = "REPLACE",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.red_light,
		color_bg2 = colors.red,
	},
	["Rv"] = {
		text = "VIRTUAL",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.purple_light,
		color_bg2 = colors.purple,
	},
	[118] = {
		text = "VISUAL",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.orange_light,
		color_bg2 = colors.orange,
	},
	[86] = {
		text = "V-LINE",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.orange_light,
		color_bg2 = colors.orange,
	},
	[22] = {
		text = "V-BLOCK",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.orange_light,
		color_bg2 = colors.orange,
	},
	[99] = {
		text = "COMMAND",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.yellow_light,
		color_bg2 = colors.yellow,
	},
	[115] = {
		text = "SELECT",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.aqua_light,
		color_bg2 = colors.aqua,
	},
	[83] = {
		text = "S-LINE",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.aqua_light,
		color_bg2 = colors.aqua,
	},
	[19] = {
		text = "S-BLOCK",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.aqua_light,
		color_bg2 = colors.aqua,
	},
	[116] = {
		text = "TERMINAL",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.green_light,
		color_bg2 = colors.green,
	},
	["rm"] = {
		text = "--MORE",
		color_fg1 = colors.background,
		color_fg2 = colors.foreground,
		color_bg1 = colors.gray_light,
		color_bg2 = colors.gray_dark,
	},
}

local get_mode = function(mode_name)
	local mode_name_byte = mode_name:byte()
	local mode = mode_table[mode_name_byte]
	local more_mode = mode_table["rm"]

	if mode == nil then
		return {
			text = mode_name,
			color_fg1 = more_mode.color_fg1,
			color_fg2 = more_mode.color_fg2,
			color_bg1 = more_mode.color_bg1,
			color_bg2 = more_mode.color_bg2,
		}
	end

	return mode
end

local function has_file_type()
	local f_type = vim.bo.filetype
	if not f_type or f_type == "" then
		return false
	end
	return true
end

gl.short_line_list = { "NvimTree" }

gls.short_line_left[1] = {
	BufferIcon = {
		provider = function()
			local buffer_type_icon = buffer.get_buffer_type_icon()
			return buffer_type_icon and " " .. buffer_type_icon or ""
		end,
		condition = has_file_type,
		highlight = { colors.foreground, colors.gray_darker },
	},
}

gls.short_line_left[2] = {
	FileTypeName = {
		provider = function()
			return vim.bo.filetype
		end,
		condition = has_file_type,
		highlight = { colors.foreground, colors.gray_darker },
	},
}

gls.left[1] = {
	ViMode = {
		provider = function()
			local mode_name = vim.fn.mode()
			local mode = get_mode(mode_name)

			vim.cmd(string.format("highlight GalaxyViMode guifg=%s guibg=%s", mode.color_fg1, mode.color_bg1))

			return "  " .. mode.text .. " "
		end,
		highlight = { colors.gray_darker, colors.gray_light, "bold" },
		separator = "",
		separator_highlight = { colors.foreground, colors.gray_darker },
	},
}

gls.left[2] = {
	FileIcon = {
		provider = function()
			return "  " .. fileinfo.get_file_icon()
		end,
		condition = glc.buffer_not_empty,
		highlight = { fileinfo.get_file_icon_color(), colors.gray_dark, "bold" },
	},
}

gls.left[3] = {
	FileName = {
		provider = { "FileName" },
		condition = glc.buffer_not_empty,
		highlight = { colors.foreground, colors.gray_dark, "bold" },
	},
}

gls.left[4] = {
	FileSize = {
		provider = function()
			return "[" .. fileinfo.get_file_size():sub(1, -2) .. "] "
		end,
		condition = glc.buffer_not_empty,
		highlight = { colors.foreground, colors.gray_dark },
		separator = "",
		separator_highlight = { colors.foreground, colors.gray_darker },
	},
}

gls.left[5] = {
	GitIcon = {
		provider = function()
			return "   "
		end,
		condition = glc.check_git_workspace,
		highlight = { colors.red, colors.gray_darker, "bold" },
	},
}

gls.left[6] = {
	GitBranch = {
		provider = vcs.get_git_branch,
		condition = glc.check_git_workspace,
		highlight = { colors.gray_lighter, colors.gray_darker, "bold" },
	},
}

gls.left[7] = {
	DiffAdd = {
		provider = vcs.diff_add,
		condition = glc.check_git_workspace,
		icon = "  樂",
		highlight = { colors.green, colors.gray_darker, "bold" },
	},
}

gls.left[8] = {
	DiffModified = {
		provider = vcs.diff_modified,
		condition = glc.check_git_workspace,
		icon = "  ﱣ ",
		highlight = { colors.yellow, colors.gray_darker, "bold" },
	},
}

gls.left[9] = {
	DiffRemove = {
		provider = vcs.diff_remove,
		condition = glc.check_git_workspace,
		icon = "   ",
		highlight = { colors.red, colors.gray_darker, "bold" },
	},
}

gls.left[10] = {
	CodeConextIcon = {
		provider = function()
			return "   "
		end,
		condition = function()
			return gps.is_available()
		end,
		highlight = { colors.blue, colors.gray_darker, "bold" },
	},
}

gls.left[11] = {
	CodeContext = {
		provider = function()
			local code_context = gps.get_location()
            if code_context == "" then
                return ""
            end
            return code_context
		end,
		condition = function()
			return gps.is_available()
		end,
		highlight = { colors.gray_lighter, colors.gray_darker },
	},
}

gls.right[1] = {
	LspClientIcon = {
		provider = function()
			return "  漣"
		end,
		condition = glc.buffer_not_empty,
		highlight = { colors.aqua, colors.gray_darker },
	},
}

gls.right[2] = {
	LspClient = {
		provider = function()
			return lspclient.get_lsp_client() .. " "
		end,
		condition = glc.buffer_not_empty,
		highlight = { colors.foreground, colors.gray_darker },
	},
}

gls.right[3] = {
	DiagnosticError = {
		provider = diagnostic.get_diagnostic_error,
		condition = glc.buffer_not_empty,
		icon = "  ",
		highlight = { colors.red, colors.gray_darker, "bold" },
	},
}

gls.right[4] = {
	DiagnosticWarn = {
		provider = diagnostic.get_diagnostic_warn,
		condition = glc.buffer_not_empty,
		icon = "  ",
		highlight = { colors.yellow, colors.gray_darker, "bold" },
	},
}

gls.right[5] = {
	DiagnosticHint = {
		provider = diagnostic.get_diagnostic_hint,
		condition = glc.buffer_not_empty,
		icon = "  ",
		highlight = { colors.green, colors.gray_darker, "bold" },
	},
}

gls.right[6] = {
	DiagnosticInfo = {
		provider = diagnostic.get_diagnostic_info,
		condition = glc.buffer_not_empty,
		icon = "  ",
		highlight = { colors.blue, colors.gray_darker, "bold" },
	},
}

gls.right[7] = {
	FileType = {
		provider = function()
			return vim.bo.filetype
		end,
		condition = has_file_type,
		icon = "   ",
		highlight = { colors.foreground, colors.gray_dark },
	},
}

gls.right[8] = {
	FileEncoding = {
		provider = function()
			return vim.bo.fileencoding
		end,
		condition = glc.buffer_not_empty,
		icon = "   ",
		highlight = { colors.foreground, colors.gray_dark },
	},
}

gls.right[9] = {
	FileFormat = {
		provider = function()
			return vim.bo.fileformat
		end,
		condition = glc.buffer_not_empty,
		icon = "   ",
		highlight = { colors.foreground, colors.gray_dark },
	},
}

gls.right[10] = {
	Position = {
		provider = function()
			return string.format(" %s:%s ", vim.fn.line("."), vim.fn.col("."))
		end,
		condition = glc.buffer_not_empty,
		icon = "  ",
		highlight = { colors.foreground, colors.gray_dark },
	},
}

gls.right[11] = {
	LinePercent = {
		provider = "LinePercent",
		condition = glc.buffer_not_empty,
		icon = " ",
		highlight = { colors.foreground, colors.gray_dark },
	},
}
