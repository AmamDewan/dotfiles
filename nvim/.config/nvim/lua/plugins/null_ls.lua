local null_ls = require("null-ls")

require("null-ls").setup({
	on_attach = require("plugins.lspconfig").on_attach,
	sources = {
		-- Formatting
		null_ls.builtins.formatting.prettierd,
		null_ls.builtins.formatting.stylua,
		null_ls.builtins.formatting.golines,
		null_ls.builtins.formatting.fish_indent,

		-- Diagnostics
		null_ls.builtins.diagnostics.write_good,
	},
})
