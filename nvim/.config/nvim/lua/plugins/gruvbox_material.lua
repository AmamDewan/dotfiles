vim.g.gruvbox_material_palette = "material"
vim.g.gruvbox_material_background = "soft"
vim.g.gruvbox_material_cursor = "orange"
vim.g.gruvbox_material_enable_bold = 1
vim.g.gruvbox_material_enable_italic = 1
vim.g.gruvbox_material_disable_italic_comment = 0
vim.g.gruvbox_material_diagnostic_text_highlight = 1
vim.g.gruvbox_material_diagnostic_line_highlight = 1
vim.g.gruvbox_material_diagnostic_virtual_text = "colored"
vim.g.gruvbox_material_current_word = "grey background"
vim.g.gruvbox_material_statusline_style = "default"
vim.g.gruvbox_material_better_performance = 1

vim.cmd("colorscheme gruvbox-material")
