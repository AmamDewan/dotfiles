vim.cmd("packadd packer.nvim")

local packer = require("packer")

packer.init {
    display = {
        open_fn = function()
            return require("packer.util").float({ border = "single" })
        end,
        prompt_border = "single"
    },
    git = {
        clone_timeout = 600 -- Timeout, in seconds, for git clones
    },
    auto_clean = true,
    compile_on_sync = true,
    auto_reload_compiled = true
}

return packer
