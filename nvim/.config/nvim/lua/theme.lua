local M = {}

M.colors = {
    background = "#32302F",
    foreground = "#D4BE98",
    red = "#C14A4A",
    red_light = "#EA6962",
    green = "#6C782E",
    green_light = "#A9B665",
    blue = "#45707A",
    blue_light = "#7DAEA3",
    aqua = "#4C7A5D",
    aqua_light = "#89B482",
    yellow = "#B47109",
    yellow_light = "#D8A657",
    orange = "#C35E0A",
    orange_light = "#E78A4E",
    pink = "#F06292",
    purple = "#945E80",
    purple_light = "#D3869B",
    gray = "#665C54",
    gray_light = "#A89984",
    gray_ligter = "#EBDBB2",
    gray_dark = "#504945",
    gray_darker = "#3C3836"
}

return M
