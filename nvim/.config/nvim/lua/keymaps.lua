local map = vim.api.nvim_set_keymap
local options = {
	noremap = true,
}

-- General
map("n", "<Space>", "", {})
map("n", "<leader><esc>", ":nohlsearch<cr>", options)
map("n", "<F2>", "<Cmd>set invpaste paste?<CR>", options)
vim.opt.pastetoggle = "<F2>"

-- Bufferline
map("n", "<Leader>b]", "<Cmd>BufferLineCycleNext<CR>", options)
map("n", "<Leader>b[", "<Cmd>BufferLineCyclePrev<CR>", options)
map("n", "<Leader>bn", "<Cmd>BufferLineMoveNext<CR>", options)
map("n", "<Leader>bp", "<Cmd>BufferLineMovePrev<CR>", options)
map("n", "<Leader>be", "<Cmd>BufferLineSortByExtension<CR>", options)
map("n", "<Leader>bd", "<Cmd>BufferLineSortByDirectory<CR>", options)
map("n", "<Leader>bt", "<Cmd>BufferLineSortByTabs<CR>", options)
map("n", "<Leader>bg", "<Cmd>BufferLinePick<CR>", options)
map("n", "<Leader>bc", "<Cmd>BufferLinePickClose<CR>", options)

-- Focus
map("n", "<Leader>sh", "<Cmd>lua require('focus').split_command('h')<CR>", options)
map("n", "<Leader>sj", "<Cmd>lua require('focus').split_command('j')<CR>", options)
map("n", "<Leader>sk", "<Cmd>lua require('focus').split_command('k')<CR>", options)
map("n", "<Leader>sl", "<Cmd>lua require('focus').split_command('l')<CR>", options)

-- Nvim Tree
map("n", "<Leader>tt", "<Cmd>lua require('nvim-tree').toggle()<CR>", options)
map("n", "<Leader>tr", "<Cmd>lua require('nvim-tree').refresh()<CR>", options)
map("n", "<Leader>tf", "<Cmd>lua require('nvim-tree').focus()<CR>", options)

-- Telescope
map("n", "<Leader>ff", "<Cmd>lua require('telescope.builtin').find_files()<CR>", options)
map("n", "<Leader>fF", "<Cmd>lua require('telescope.builtin').find_files({hidden=true})<CR>", options)
map("n", "<Leader>fb", "<Cmd>lua require('telescope').extensions.file_browser.file_browser()<CR>", options)
map("n", "<Leader>fB", "<Cmd>lua require('telescope.builtin').file_browser({hidden=true})<CR>", options)
map("n", "<Leader>fr", "<Cmd>lua require('telescope.builtin').oldfiles()<CR>", options)
map("n", "<Leader>fs", "<Cmd>lua require('telescope.builtin').live_grep()<CR>", options)
map(
	"n",
	"<Leader>fS",
	"<Cmd>lua require('telescope.builtin').live_grep({additional_args=function(opts) return '--hidden' end})<CR>",
	options
)
map("n", "<Leader>fg", "<Cmd>lua require('telescope.builtin').git_files()<CR>", options)
map("n", "<Leader>fp", "<Cmd>lua require('telescope._extensions').manager.projects.projects()<CR>", options)
map("n", "<Leader>fc", "<Cmd>lua require('telescope.builtin').command_history()<CR>", options)
map("n", "<Leader>ft", "<Cmd>lua require('telescope.builtin').buffers()<CR>", options)
map("n", "<Leader>fh", "<Cmd>lua require('telescope.builtin').help_tags()<CR>", options)
map("n", "<Leader>fM", "<Cmd>lua require('telescope.builtin').man_pages()<CR>", options)
map("n", "<Leader>fm", "<Cmd>lua require('telescope.builtin').marks()<CR>", options)
map("n", "<Leader>ls", "<Cmd>lua require('telescope.builtin').lsp_document_symbols()<CR>", options)
map("n", "<Leader>ld", "<Cmd>lua require('telescope.builtin').lsp_definitions({jump_type='never'})<CR>", options)
map("n", "<Leader>lD", "<Cmd>lua require('telescope.builtin').lsp_document_diagnostics()<CR>", options)
map("n", "<Leader>li", "<Cmd>lua require('telescope.builtin').lsp_implementations()<CR>", options)
map("n", "<Leader>lr", "<Cmd>lua require('telescope.builtin').lsp_references()<CR>", options)
map("n", "<Leader>la", "<Cmd>lua require('telescope.builtin').lsp_code_actions()<CR>", options)
map("n", "<Leader>ts", "<Cmd>lua require('telescope.builtin').treesitter()<CR>", options)
map("n", "<Leader>td", "<Cmd>lua require('telescope._extensions').manager['todo-comments'].todo()<CR>", options)
map("n", "<Leader>bs", "<Cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>", options)
map("n", "<Leader>gc", "<Cmd>lua require('telescope.builtin').git_commits()<CR>", options)
map("n", "<Leader>gC", "<Cmd>lua require('telescope.builtin').git_bcommits()<CR>", options)
map("n", "<Leader>gb", "<Cmd>lua require('telescope.builtin').git_branches()<CR>", options)
map("n", "<Leader>gs", "<Cmd>lua require('telescope.builtin').git_status()<CR>", options)
map("n", "<Leader>gS", "<Cmd>lua require('telescope.builtin').git_stash()<CR>", options)
map("n", "<Leader>ss", "<Cmd>lua require('telescope.builtin').spell_suggest()<CR>", options)
map("n", "<Leader>jL", "<Cmd>lua require('telescope.builtin').jumplist()<CR>", options)

-- Persistence
map("n", "<Leader>Sd", [[<Cmd>lua require("persistence").load()<CR>]], options)
map("n", "<Leader>Sl", [[<Cmd>lua require("persistence").load({ last = true })<CR>]], options)

-- ToggleTerm
map("n", "<C-\\>", "<Cmd>lua require('toggleterm').toggle(1)<CR>", options)
map("n", "<A-\\>t", "<Cmd>lua require('toggleterm').toggle(1)<CR>", options)
map("n", "<A-\\>v", "<Cmd>lua require('toggleterm').toggle(1, _, _, 'vertical')<CR>", options)
map("n", "<A-\\>h", "<Cmd>lua require('toggleterm').toggle(1, _, _, 'horizontal')<CR>", options)
map("n", "<A-\\>f", "<Cmd>lua require('toggleterm').toggle(1, _, _, 'float')<CR>", options)

-- Spectre
map("n", "<Leader>St", "<Cmd>lua require('spectre').open()<CR>", options)
map("v", "<Leader>Sv", "<Cmd>lua require('spectre').open_visual()<CR>", options)
map("n", "<Leader>Sw", "<Cmd>lua require('spectre').open_visual({select_word=true})<CR>", options)
map("n", "<Leader>Sf", "<Cmd>lua require('spectre').open_file_search()<CR>", options)

function _G.set_terminal_keymaps()
	local opts = { noremap = true }
	vim.api.nvim_buf_set_keymap(0, "t", "<esc>", [[<C-\><C-n>]], opts)
	vim.api.nvim_buf_set_keymap(0, "t", "jk", [[<C-\><C-n>]], opts)
	vim.api.nvim_buf_set_keymap(0, "t", "<C-h>", [[<C-\><C-n><C-W>h]], opts)
	vim.api.nvim_buf_set_keymap(0, "t", "<C-j>", [[<C-\><C-n><C-W>j]], opts)
	vim.api.nvim_buf_set_keymap(0, "t", "<C-k>", [[<C-\><C-n><C-W>k]], opts)
	vim.api.nvim_buf_set_keymap(0, "t", "<C-l>", [[<C-\><C-n><C-W>l]], opts)
end

-- if you only want these mappings for toggle term use term://*toggleterm#* instead
vim.cmd("autocmd! TermOpen term://* lua set_terminal_keymaps()")

-- Minimap
map("n", "<Leader>mm", "<Cmd>MinimapToggle<CR>", options)

-- SymbolsOutline
map("n", "<Leader>so", "<Cmd>lua require('symbols-outline').toggle_outline()<CR>", options)

-- Hop
map("n", "<Leader>jw", "<Cmd>lua require('hop').hint_words()<CR>", options)
map("n", "<Leader>jl", "<Cmd>lua require('hop').hint_lines()<CR>", options)

-- Git
map("n", "<Leader>lb", "<Cmd>lua require('gitsigns').toggle_current_line_blame()<CR>", options)
map("n", "<Leader>gds", "<Cmd>Gvdiffsplit!<CR>", options)
map("n", "<Leader>gdh", "<Cmd>diffget //2<CR>", options)
map("n", "<Leader>gdl", "<Cmd>diffget //3<CR>", options)
