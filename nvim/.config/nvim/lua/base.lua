-- User Interface
vim.opt.background = "dark"
vim.opt.termguicolors = true
vim.opt.showtabline = 2
vim.opt.laststatus = 2
vim.opt.title = true
vim.opt.mouse = "a"
vim.opt.cursorline = true
vim.opt.cursorcolumn = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.ruler = false
vim.opt.fcs = "eob: "
vim.opt.wildmenu = true
vim.opt.tabpagemax = 50
vim.opt.errorbells = false
vim.opt.visualbell = true
vim.opt.modeline = false
vim.opt.showmode = false
vim.opt.scl = "yes"

-- Indention
vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.smarttab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.shiftround = true
vim.opt.expandtab = true

-- Search
vim.opt.hlsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.incsearch = true

-- Performance
vim.opt.complete = vim.opt.complete - "i"
vim.opt.lazyredraw = true
vim.opt.updatetime = 250

-- Text Rendering
vim.opt.display = vim.opt.display + "lastline"
vim.opt.encoding = "utf-8"
vim.opt.linebreak = true
vim.opt.scrolloff = 1
vim.opt.sidescrolloff = 5
vim.opt.wrap = true
vim.cmd("syntax enable")

-- Folding
vim.opt.foldenable = true
vim.opt.foldmethod = "expr"
vim.opt.foldnestmax = 3
vim.opt.completeopt = "menuone,noselect"

-- Tree
vim.g.loaded_netrwPlugin = 1

-- Miscellaneous
vim.opt.clipboard = vim.opt.clipboard + "unnamedplus"
vim.opt.autoread = true
vim.opt.backspace = "indent,eol,start"
vim.opt.confirm = true
vim.opt.writebackup = true
vim.opt.formatoptions = vim.opt.formatoptions + "j"
vim.opt.hidden = true
vim.opt.history = 1000
vim.opt.swapfile = false
vim.opt.nrformats = vim.opt.nrformats - "octal"
vim.opt.shell = "fish"
vim.opt.spell = false
vim.opt.wildignore = vim.opt.wildignore + ".pyc,.swp"
vim.g.mapleader = " "
vim.g.python3_host_prog = "$NVIM_PYTHON3_HOST_EXECUTABLE_PATH"
vim.g.node_host_prog = "$NVIM_NODE_HOST_EXECUTABLE_PATH"
