# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import List  # noqa: F401

from libqtile import qtile, bar, layout, widget, extension, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

import os
import subprocess
import re


mod = "mod4"
terminal = guess_terminal()
font_name = "InconsolataGo Nerd Font Mono"
font_size = 14
icon_font_name = "InconsolataGo Nerd Font Mono"
icon_font_size = 24
margin = 8
border_width = 2

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

@lazy.function
def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

@lazy.function
def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)


def move_window_to_screen(qtile, screen_index):
    group = qtile.screens[screen_index].group.name
    qtile.current_window.togroup(group)
    lazy.to_screen(screen_index) # Not focusing on the screen for some reason


def get_volume_mixer_output():
    return subprocess.run(["amixer", "-D", "pipewire", "sget", "Master"], capture_output=True).stdout.decode()


def get_volume():
    re_vol = re.compile(r"(\d?\d?\d?)%")
    mixer_out = get_volume_mixer_output()
    volume_search = re_vol.search(mixer_out)

    if volume_search:
        return volume_search[0]


def set_volume(_, percent: int, mode: str, notify: bool = True):
    m = "+"
    vol_icon = "ﱛ"

    if mode == "d":
        m = "-"
        vol_icon = "ﱜ"

    is_volume_muted = get_volume_toggle_status()

    if is_volume_muted:
        toggle_volume(_, False)

    subprocess.run(["amixer", "-q", "-D", "pipewire", "set", "Master", f"{int(percent)}%{m}"])

    if notify:
        volume = get_volume()
        subprocess.run(["dunstify", "-a", "setVolume", "-r", "991049", "-t", "3000", "-h", f"int:value:{volume}", f"Volume {vol_icon}"])


def get_volume_toggle_status() -> bool:
    mixer_out = get_volume_mixer_output()

    return "[off]" in mixer_out


def toggle_volume(_, notify: bool = True):
    subprocess.run(["amixer", "-q", "-D", "pipewire", "set", "Master", "toggle"])

    if notify:
        is_volume_muted = get_volume_toggle_status()
        if is_volume_muted:
            vol_icon = "ﱝ"
            volume = "0"
        else:
            vol_icon = ""
            volume = get_volume()
        subprocess.run(["dunstify", "-a", "setVolume", "-r", "991049", "-t", "3000", "-h", f"int:value:{volume}", f"Volume {vol_icon}"])


### Colors ###

# Key: Color name
# Value: Color hex code or list of color hex codes for gradient
color_scheme = {
    "background": "#32302F",
    "foreground": "#D4BE98",
    "red": "#C14A4A",
    "green": "#6C782E",
    "blue": "#45707A",
    "yellow": "#B47109",
    "orange": "#C35E0A",
    "pink": "#F06292",
    "purple": "#945E80",
    "gray": "#665C54",
    "light_gray": "#D4BE98",
    "dark_gray": "#504945"
}
color_theme = color_scheme.get("orange")

### Keys ###

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    Key([mod, "shift"], "space", lazy.layout.flip(), desc="Flip layout"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod], "i", lazy.layout.grow(), desc="Grow current window"),
    Key([mod], "u", lazy.layout.shrink(), desc="Shrink current window"),
    Key([mod], "m", lazy.layout.maximize(), desc="Maximize current window"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "shift"], "r", lazy.reload_config(), desc="Reload Qtile config"),
    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

    Key([mod], "r", lazy.spawn("rofi -modi run -modi run -show run"), desc="Run a command"),
    Key([mod], "a", lazy.spawn("rofi -modi run -modi drun -show drun"), desc="Run an application"),

    # Focus screens
    Key([mod, "mod1"], "1", lazy.to_screen(0), desc="Focus screen 1"),
    Key([mod, "mod1"], "2", lazy.to_screen(1), desc="Focus screen 2"),
    
    # Show all windows
    Key([mod], "v", lazy.run_extension(extension.WindowList(dmenu_command="rofi -dmenu", dmenu_prompt="Windows"))),

    # Toggle fullscreen
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen"),

    # Move windows
    Key([mod, "control"], "1", lazy.function(move_window_to_screen, 0), desc="Move window to first screen"),
    Key([mod, "control"], "2", lazy.function(move_window_to_screen, 1), desc="Move window to second screen"),

    # Multimedia

    ## System volume
    Key([], "XF86AudioRaiseVolume", lazy.function(set_volume, 2, "i"), desc="Increase system volume"),
    Key([], "XF86AudioLowerVolume", lazy.function(set_volume, 2, "d"), desc="Decrease system volume"),
    Key([], "XF86AudioMute", lazy.function(toggle_volume), desc="Mute system volume"),

    ## MPD volume
    Key([mod, "shift"], "p", lazy.spawn("mpc toggle"), desc="Toggle MPD play and pause"),
    Key([mod, "shift"], "bracketleft", lazy.spawn("mpc prev"), desc="Play previous song in MPD"),
    Key([mod, "shift"], "bracketright", lazy.spawn("mpc next"), desc="Play next song in MPD"),
    Key([mod, "shift"], "equal", lazy.spawn("mpc volume +5"), desc="Increase MPD volume"),
    Key([mod, "shift"], "minus", lazy.spawn("mpc volume -5"), desc="Increase MPD volume"),

    ## Utilities
    Key([mod, "shift"], "x", lazy.spawn("keepmenu")),
    Key([mod, "shift"], "y", lazy.spawn("ytfzf -D")),
    Key([mod, "shift"], "e", lazy.spawn("rofimoji"))
]


### Groups ###

group_name_list = ["", "", "", "", ""]
groups = [
    Group(
        name=str(index),
        label=value
    ) for index, value in enumerate(group_name_list, start=1)
]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(toggle=True),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])


### Layouts ###

default_layout_config = {
    "border_normal": color_scheme.get("dark_gray"), # Border colour(s) for un-focused windows
    "border_focus": color_theme, # Border colour(s) for the focused window
    "border_focus_stack": color_theme, # Border colour(s) for the focused window in stacked columns
    "border_on_single": True, # Draw a border when there is one only window
    "border_width": border_width, # Border width
    "margin": margin, # Margin of the layout (int or list of ints [N E S W])
    "margin_on_single": margin, # Margin when only one window. (int or list of ints [N E S W])
}

layouts = [
    layout.MonadTall(**default_layout_config),
    layout.Max(),
    # layout.Columns(**default_layout_config),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font=font_name,
    fontsize=font_size,
    foreground=color_scheme.get("foreground"),
    margin=3,
    padding=16
)
extension_defaults = widget_defaults.copy()


### Bars ###

icon_widget_config = {
    "font": icon_font_name,
    "fontsize": icon_font_size
}

groupbox_widget_config = {
    **widget_defaults,
    **icon_widget_config,
    "active": color_theme, # Active group font colour
    "inactive": color_scheme.get("light_gray"), # Inactive group font colour
    "this_current_screen_border": color_theme, # Border or line colour for group on this screen when focused
    "this_screen_border": color_scheme.get("blue"), # Border or line colour for group on this screen when unfocused
    "other_screen_border": color_scheme.get("dark_gray"), # Border or line colour for group on other screen when unfocused 
    "other_current_screen_border": color_scheme.get("red"), # Border or line colour for group on other screen when focused
    "block_highlight_text_color": color_scheme.get("foreground"), # Selected group font colour
    "highlight_method": "block", # Method of highlighting (‘border’, ‘block’, ‘text’, or ‘line’) Uses *_border color settings
    "margin_x": 0, # X Margin. Overrides ‘margin’ if set
    "rounded": False, # To round or not to round box borders,
    "disable_drag": True # Disable dragging and dropping of group names on widget
}

def get_default_bar_widget_list():
    widget_list = [
        widget.Spacer(length=margin),
        widget.GroupBox(**groupbox_widget_config),
        widget.WindowName(),
        widget.Chord(
            chords_colors={
                'launch': ("#ff0000", "#ffffff"),
            },
            name_transform=lambda name: name.upper(),
        )
    ]

    return widget_list

def get_primary_screen_bar_widget_list():
    default_widget_list = get_default_bar_widget_list()
    widget_list = default_widget_list + [
        widget.Mpd2(
            color_progress=color_theme,
            idle_message="IDLE",
            no_connection="",
            play_states={
                "play": "",
                "pause": "",
                "stop": ""
            }
        ),
        # Show IP
        widget.GenPollUrl(
            url="http://ident.me",
            json=False,
            parse=lambda resp_body: resp_body.decode(),
            update_interval=10
        ),
        widget.Net(
            format="↓ {down} ↑ {up}",
            use_bits=True
        ),
        widget.OpenWeather(
            cityid=1185241, # Dhaka, BD - city id
            format="{main_temp} °{units_temperature} {humidity}% {weather}",
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(f"{terminal} --hold bash -c 'curl -s wttr.in | head -n -2'")
            }
        ),
        widget.Volume(
            device = "pipewire"
        ),
        widget.Clock(format="%Y-%m-%d %A %I:%M %p"),
        widget.Systray(padding=margin),
        widget.Spacer(length=margin)
    ]

    return widget_list



class DefaultBar(bar.Bar):

    def __init__(self, widgets=get_default_bar_widget_list(), size=28):
        super().__init__(widgets, size)
        self.background = color_scheme.get("background")


class PrimaryScreenBar(DefaultBar):

    def __init__(self):
        super().__init__(get_primary_screen_bar_widget_list())


class LargeScreenBar(DefaultBar):
    pass   


### Screens ###


class DefaultScreen(Screen):

    def __init__(self):
        super().__init__()

 
class PrimaryScreen(DefaultScreen):

    def __init__(self):
        super().__init__()
        self.top = PrimaryScreenBar()


class LargeScreen(DefaultScreen):

    def __init__(self):
        super().__init__()
        self.bottom = LargeScreenBar()


screens = [PrimaryScreen(), LargeScreen(), DefaultScreen()]



### Hooks ###

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
], **default_layout_config)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
