function fish_greeting
    neofetch
end

function set_syntax_highlighting
    set -U fish_color_command 6C782E
    set -U fish_color_error C14A4A
    set -U fish_color_cancel C14A4A
end

set SHELL (which fish)
set_syntax_highlighting
alias v=nvim

# Starship prompt
starship init fish | source

# Zoxide
zoxide init fish | source

# pyenv
status is-login; and pyenv init --path | source
status is-interactive; and pyenv init - | source
status is-interactive; and pyenv virtualenv-init - | source
